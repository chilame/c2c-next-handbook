import React from 'react';
import { Zoom, Fab, useScrollTrigger, SvgIcon } from '@material-ui/core';

import useStyles from './scroll-top-button.styles';

export type Props = {
  positionIndex?: number;
};

const ScrollTopButton: React.FC<Props> = (props: Props) => {
  const classes = useStyles(props);
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event: React.MouseEvent<HTMLDivElement>): void => {
    const anchor = ((event.target as HTMLDivElement).ownerDocument || document).querySelector(
      '#back-to-top-anchor',
    );

    if (anchor) {
      anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role="presentation" className={classes.root}>
        <Fab
          variant="round"
          color="secondary"
          size="medium"
          aria-label="scroll back to top"
          title="TOP"
        >
          <SvgIcon width="24" height="24" viewBox="0 0 24 24">
            <path
              fill="#4D7293"
              d="M17.585 16C18.36 16 19 15.39 19 14.646c0-.365-.126-.67-.395-.923l-5.4-5.21c-.379-.35-.743-.513-1.201-.513-.45 0-.822.156-1.194.513l-5.407 5.21c-.269.26-.403.558-.403.923C5 15.39 5.64 16 6.423 16c.395 0 .759-.156 1.051-.447l4.538-4.42 4.522 4.42c.292.298.664.447 1.051.447z"
            />
          </SvgIcon>
        </Fab>
      </div>
    </Zoom>
  );
};

export default ScrollTopButton;
