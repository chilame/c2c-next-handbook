import { makeStyles } from '@material-ui/core/styles';

import { Props } from './scroll-top-button.component';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    bottom: ({ positionIndex }: Props) =>
      theme.spacing(4 + (positionIndex ? positionIndex * 16 : 0)),
    right: theme.spacing(4),
    color: '#4d7293',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
}));

export default useStyles;
