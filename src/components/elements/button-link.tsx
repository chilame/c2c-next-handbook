import React from 'react';
import CustomLink from './custom-link';
import { linkInterface } from 'utils/types';
import useStyles from '@styles/elements/button-link';

interface ButtonLinkProps {
  link: linkInterface;
  rightIcon?: React.ReactNode;
  maxWidth?: number;
}

const ButtonLink: React.FC<ButtonLinkProps> = ({ link, rightIcon, maxWidth }: ButtonLinkProps) => {
  const classes = useStyles();

  return (
    <CustomLink link={link}>
      <div className={classes.children}>
        <div style={{ maxWidth }} className={classes.text}>
          {link.text}
        </div>
        {rightIcon}
      </div>
    </CustomLink>
  );
};

export default ButtonLink;
