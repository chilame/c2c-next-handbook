import React from 'react';
import Carousel, { PagingDots, CarouselSlideRenderControlProps } from 'nuka-carousel';

type CustomCarouselProps = {
  children?: React.ReactNode;
  index?: number;
  setSlideIndex?: (index: number) => void;
  vertical?: boolean;
  invisibleChildren?: React.ReactNode;
};

const renderHorizontalBack = (props: CarouselSlideRenderControlProps) =>
  props.slideCount > 1 &&
  props.currentSlide !== 0 && (
    <div onClick={props.previousSlide} className="p-12px rounded-full link-hover cursor-pointer">
      <img alt="horizontal-back" src="/images/horizontal-back.svg" />
    </div>
  );

const renderHorizontalNext = (props: CarouselSlideRenderControlProps) =>
  props.slideCount > 1 &&
  props.slideCount > props.currentSlide + 1 && (
    <div onClick={props.nextSlide} className="p-12px rounded-full link-hover cursor-pointer">
      <img alt="horizontal-next" src="/images/horizontal-next.svg" />
    </div>
  );

const renderVerticalBack = (props: CarouselSlideRenderControlProps) =>
  props.slideCount > 1 &&
  props.currentSlide !== 0 && (
    <div onClick={props.previousSlide} className="p-12px rounded-full link-hover cursor-pointer">
      <img alt="vertical-back" src="/images/vertical-back.svg" />
    </div>
  );

const renderVerticalNext = (props: CarouselSlideRenderControlProps) =>
  props.slideCount > 1 &&
  props.slideCount > props.currentSlide + 1 && (
    <div onClick={props.nextSlide} className="p-12px rounded-full link-hover cursor-pointer">
      <img alt="vertical-next" src="/images/vertical-next.svg" />
    </div>
  );

const renderPagingDots = (props: CarouselSlideRenderControlProps) =>
  props.slideCount > 1 && (
    <div className="custom-paging-dots">
      <PagingDots {...props} />
    </div>
  );

// eslint-disable-next-line react/display-name
const renderPagingNumbers = (invisibleChildren: React.ReactNode) => (
  props: CarouselSlideRenderControlProps,
) =>
  invisibleChildren && props.slideCount > 1 ? (
    <div className="flex flex-col items-center">
      {invisibleChildren}
      <div className="flex pointer-events-auto">
        <img
          alt="horizontal-back"
          src="/images/horizontal-back.svg"
          onClick={props.previousSlide}
        />
        <div className="mx-24px notoSerifJP flex items-end leading-none font-medium">
          <div className="fs-40">{(props.currentSlide + 1).toString().padStart(2, '0')}</div>
          <div className="fs-20 ml-4px">/ {props.slideCount.toString().padStart(2, '0')}</div>
        </div>
        <img alt="horizontal-next" src="/images/horizontal-next.svg" onClick={props.nextSlide} />
      </div>
    </div>
  ) : null;

const CustomCarousel: React.FC<CustomCarouselProps> = ({
  children,
  index,
  setSlideIndex,
  vertical,
  invisibleChildren,
}: CustomCarouselProps) => {
  return (
    <Carousel
      cellAlign="center"
      dragging
      disableEdgeSwiping
      slidesToShow={1}
      slidesToScroll={1}
      vertical={vertical}
      slideIndex={index}
      afterSlide={(slideIndex) => setSlideIndex && setSlideIndex(slideIndex)}
      renderCenterLeftControls={vertical ? renderPagingDots : renderHorizontalBack}
      renderCenterRightControls={vertical ? null : renderHorizontalNext}
      renderTopCenterControls={
        vertical ? renderVerticalBack : renderPagingNumbers(invisibleChildren)
      }
      renderBottomCenterControls={vertical ? renderVerticalNext : renderPagingDots}
    >
      {children}
    </Carousel>
  );
};

export default CustomCarousel;
