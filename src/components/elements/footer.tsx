import React from 'react';
import { useRouter } from 'next/router';
import Image from './image';
import CustomLink from './custom-link';
import { linkInterface, mediaInterface, menuInterface } from 'utils/types';
import useStyles from '@styles/elements/footer';

interface FooterProps {
  footer: {
    logo: mediaInterface;
    button: linkInterface;
    extraText: string;
  };
  menu: menuInterface;
}

interface SwitchButtonProps {
  link: linkInterface;
}

const SwitchButton: React.FC<SwitchButtonProps> = ({ link }: SwitchButtonProps) => {
  if (!link) {
    return null;
  }

  const classes = useStyles();
  return (
    <CustomLink link={link}>
      <div className={classes.switchButton}>
        <img src="/images/switch-salomie.svg" alt="switch-salomie" />
        <div className={classes.switchButtonText}>{link.text}</div>
      </div>
    </CustomLink>
  );
};

const Footer: React.FC<FooterProps> = ({ footer, menu }: FooterProps) => {
  const classes = useStyles();
  const router = useRouter();
  const { logo, extraText } = footer || {};
  const { links = [], therapist, customer } = menu || {};

  return (
    <footer className={classes.footer}>
      <div className={classes.mainFooter}>
        {/* Logo */}
        <Image media={logo} />
        {/* Links */}
        <nav className={classes.nav}>
          {links.map((link) => (
            <div key={link.id} className={classes.link}>
              <CustomLink link={link}>
                <div className={classes.linkText}>{link.text}</div>
              </CustomLink>
            </div>
          ))}
        </nav>
        {/* Switch button */}
        {router.asPath === '/therapist' ? (
          <SwitchButton link={customer} />
        ) : (
          <SwitchButton link={therapist} />
        )}
      </div>
      {/* Extra text */}
      <div className={classes.extraText}>{extraText}</div>
    </footer>
  );
};

export default Footer;
