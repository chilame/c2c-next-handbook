import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Image from './image';
import MenuDrawer from './menu';
import CustomLink from './custom-link';
import { linkInterface, mediaInterface } from '@utils/types';
import useStyles from '@styles/elements/header';

interface HeaderProps {
  header: {
    logo: mediaInterface;
  };
  menu: {
    links: linkInterface[];
    customer: linkInterface;
    therapist: linkInterface;
  };
}

interface HeaderItemProps {
  link: linkInterface;
  imgName: string;
}

const HeaderItem: React.FC<HeaderItemProps> = ({ link, imgName }: HeaderItemProps) => {
  if (!link) {
    return null;
  }

  const classes = useStyles();
  return (
    <div className={classes.headerItem}>
      <CustomLink link={link}>
        <div className={classes.link}>
          <img alt={imgName} src={`/images/${imgName}.svg`} width={24} height={24} />
          <div className={classes.linkText}>{link.text}</div>
        </div>
      </CustomLink>
    </div>
  );
};

const Header: React.FC<HeaderProps> = ({ header, menu }: HeaderProps) => {
  const classes = useStyles();
  const router = useRouter();
  const [menuOpen, setMenuOpen] = React.useState(false);
  const { logo } = header || {};
  const { therapist, customer, links = [] } = menu || {};

  return (
    <>
      <nav className={classes.nav}>
        <Link href="/[[...slug]]" as="/">
          <a>
            <Image media={logo} className={classes.logo} />
          </a>
        </Link>
        <div className={classes.links}>
          {router.asPath === '/therapist' ? (
            <HeaderItem link={customer} imgName="switch-blue" />
          ) : (
            <HeaderItem link={therapist} imgName="switch-blue" />
          )}
          {links.length !== 0 && (
            <div className={classes.hamburger} onClick={() => setMenuOpen(!menuOpen)}>
              <img alt="hamburger" src="/images/hamburger.svg" />
            </div>
          )}
        </div>
      </nav>
      <MenuDrawer toggleMenuDrawer={setMenuOpen} openState={menuOpen} links={links} logo={logo} />
    </>
  );
};

export default Header;
