import React from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Drawer } from '@material-ui/core';
import Image from './image';
import CustomLink from './custom-link';
import { linkInterface, mediaInterface } from '@utils/types';

interface MenuDrawerProps {
  toggleMenuDrawer: (openState: boolean) => void;
  openState: boolean;
  links: linkInterface[];
  logo: mediaInterface;
}

const closeDrawer = (setState: (openState: boolean) => void) => (
  event: React.KeyboardEvent | React.MouseEvent,
): void => {
  if (
    event.type === 'keydown' &&
    ((event as React.KeyboardEvent).key === 'Tab' || (event as React.KeyboardEvent).key === 'Shift')
  ) {
    return;
  }

  setState(false);
};

const MenuDrawer: React.FC<MenuDrawerProps> = ({
  toggleMenuDrawer,
  openState,
  links,
  logo,
}: MenuDrawerProps) => {
  const router = useRouter();

  return (
    <div
      role="presentation"
      onClick={closeDrawer(toggleMenuDrawer)}
      onKeyDown={closeDrawer(toggleMenuDrawer)}
    >
      <Drawer anchor="right" open={openState}>
        {/* Close button */}
        <div className="flex justify-between" id="menu-drawer-header">
          <div>
            <Link href="/[[...slug]]" as="/">
              <a>
                <Image media={logo} className="object-contain mobile-component cursor-pointer" />
              </a>
            </Link>
          </div>
          <div
            id="menu-drawer-button"
            className="flex flex-col items-center justify-center cursor-pointer link-hover header-item"
          >
            <img src="/images/close.svg" alt="close" />
          </div>
        </div>
        {/* Menu */}
        <div id="menu-drawer-links">
          {links.map((link) => {
            const { id, url, text } = link;
            const active = url === router.asPath;
            const activeClassName = active ? 'menu-drawer-link-active' : '';

            return (
              <CustomLink key={id} link={link}>
                <div className="fs-16 air-force-blue-color font-medium link-hover menu-drawer-link cursor-pointer">
                  <span className={activeClassName}>{text}</span>
                </div>
              </CustomLink>
            );
          })}
        </div>
      </Drawer>
    </div>
  );
};

export default MenuDrawer;
