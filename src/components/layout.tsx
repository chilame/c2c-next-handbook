import React from 'react';
import Header from './elements/header';
import Footer from './elements/footer';
import { linkInterface, mediaInterface, menuInterface } from 'utils/types';

interface LayoutProps {
  children: React.ReactNode;
  global: {
    header: {
      logo: mediaInterface;
    };
    menu: menuInterface;
    footer: {
      logo: mediaInterface;
      links?: Array<linkInterface>;
      button: linkInterface;
      extraText: string;
    };
  };
}

const Layout: React.FC<LayoutProps> = ({ children, global }: LayoutProps) => {
  const { header, footer, menu } = global;

  return (
    <div className="flex flex-col justify-between min-h-screen">
      {/* Aligned to the top */}
      <div className="flex-1">
        {(header || menu) && <Header header={header} menu={menu} />}
        <div>{children}</div>
      </div>
      {/* Aligned to the bottom */}
      {(footer || menu) && <Footer footer={footer} menu={menu} />}
    </div>
  );
};

export default Layout;
