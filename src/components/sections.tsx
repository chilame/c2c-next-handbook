import React from 'react';
import { useRouter } from 'next/router';
import BookSection from './sections/book';
// Map Strapi sections to section components
const sectionComponents: { [key: string]: React.ReactNode } = {
  'sections.book-section': BookSection,
};

// Display a section individually
const Section = ({ sectionData }: { sectionData: { __component: string } }) => {
  // Prepare the component
  const SectionComponent: any = sectionComponents[sectionData.__component];

  if (!SectionComponent) {
    return null;
  }

  // Display the section
  return <SectionComponent data={sectionData} />;
};

const PreviewModeBanner = () => {
  const router = useRouter();
  const exitURL = `/api/exit-preview?redirect=${encodeURIComponent(router.asPath)}`;

  return (
    <div className="py-4 bg-red-600 text-red-100 font-semibold uppercase tracking-wide">
      <div className="container">
        Preview mode is on.{' '}
        <a className="underline" href={`/api/exit-preview?redirect=${router.asPath}`}>
          Turn off
        </a>
      </div>
    </div>
  );
};

interface SectionsProps {
  sections: {
    id: string | number;
    __component: string;
  }[];
  preview: boolean;
}

// Display the list of sections
const Sections: React.FC<SectionsProps> = ({ sections, preview }: SectionsProps) => {
  return (
    <div>
      {/* Show a banner if preview mode is on */}
      {preview && <PreviewModeBanner />}
      {/* Show the actual sections */}
      {sections.map((section) => (
        <Section sectionData={section} key={`${section.__component}${section.id}`} />
      ))}
    </div>
  );
};

export default Sections;
