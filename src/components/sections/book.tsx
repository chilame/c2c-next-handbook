import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

import { backgroundColorInterface, textColorInterface } from 'utils/types';
import useStyles from '@styles/sections/book';

interface FeatureProps {
  data: {
    title: string;
    subTitle: string;
    description: string;
    chapters: {
      id: string | number;
      name: string;
      content: string;
    }[];
    backgroundColor: backgroundColorInterface;
    textColor: textColorInterface;
  };
}

const FeatureSection: React.FC<FeatureProps> = ({ data }: FeatureProps) => {
  const classes = useStyles();
  const router = useRouter();
  const { query } = router;
  const path = router.asPath.split('?')[0];
  const { chapters } = data;
  console.log('🚀 ~ file: book.tsx ~ line 29 ~ router', router);
  const validQuery = query.chapter && chapters.length >= parseInt(query.chapter as string);

  useEffect(() => {
    if (query.chapter && chapters.length < parseInt(query.chapter as string)) {
      const path = router.asPath.split('?')[0];
      router.push(path);
    }
  }, [query]);

  const handleOpenChapter = (chapterNumber: number) => {
    router.push(`/[[...slug]]?chapter=${chapterNumber}`, `${path}?chapter=${chapterNumber}`, {
      shallow: true,
    });
  };

  const renderRightSide = () => {
    if (validQuery) {
      const chapNum = parseInt(query.chapter as string) - 1;
      window.scrollTo(0, 0);
      return (
        <>
          <div dangerouslySetInnerHTML={{ __html: chapters[chapNum].content }}></div>
          {chapNum + 1 < chapters.length && (
            <button className={classes.nextChap} onClick={() => handleOpenChapter(chapNum + 2)}>{`${
              chapters[chapNum + 1].name
            } →`}</button>
          )}
        </>
      );
    }

    return (
      <>
        <h1 className={classes.title}>{data.title}</h1>
        <p className={classes.subTitle}>{data.subTitle}</p>
        <hr />
        <div dangerouslySetInnerHTML={{ __html: data.description }}></div>
        <hr />
        <ul className={classes.bookContent}>
          {chapters.map((item, index) => (
            <li key={item.id} className={classes.chapterContent}>
              <p className={classes.chapterNumber}>{`Chapter ${index + 1}`}</p>
              <h3 className={classes.chapterName}>
                <a onClick={() => handleOpenChapter(index + 1)}>{item.name}</a>
              </h3>
            </li>
          ))}
        </ul>
      </>
    );
  };

  const renderLeftSide = () => {
    if (validQuery) {
      const chapterNum = parseInt(query.chapter as string);
      return (
        <>
          <p className={classes.chapterNumber}>{`Chapter ${chapterNum}`}</p>
          <h3 className={classes.chapterTitle}>{chapters[chapterNum - 1].name}</h3>
          {chapterNum < chapters.length && (
            <h3 className={`${classes.chapterName} ${classes.alignRight}`}>
              <a
                onClick={() => handleOpenChapter(chapterNum + 1)}
              >{`Next: ${chapters[chapterNum].name}`}</a>
            </h3>
          )}
        </>
      );
    }
    return null;
  };

  return (
    <div className={classes.bookSection}>
      <div className={classes.leftSide}>
        <div className={classes.menu}>{renderLeftSide()}</div>
      </div>
      <div className={classes.rightSide}>{renderRightSide()}</div>
    </div>
  );
};

export default FeatureSection;
