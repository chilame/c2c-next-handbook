import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  children: {
    display: 'flex',
    padding: '12px',
    backgroundColor: '#ffd280',
    borderRadius: '4px',
  },
  text: {
    textAlign: 'left',
    fontWeight: 700,
    color: '#4d7293',
  },
}));

export default useStyles;
