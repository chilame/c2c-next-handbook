import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  footer: {
    color: '#a7b5c0',
  },
  mainFooter: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#233340',
    paddingTop: '16px',
    paddingBottom: '24px',
  },
  nav: {
    margin: '16px 0',
    padding: '0 16px',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    '& > div:not(:first-child) > a': {
      borderLeft: '1px solid #a7b5c0',
    },
  },
  link: {
    display: 'flex',
    padding: '8px 0',
  },
  linkText: {
    padding: '0 16px',
    fontSize: '12px',
  },
  extraText: {
    fontSize: '10px',
    textAlign: 'center',
    padding: '4px 0',
    backgroundColor: '#293e4e',
  },
  switchButton: {
    padding: '6px 16px',
    fontSize: '12px',
    display: 'flex',
    border: '1px solid #ffd280',
    borderRadius: '4px',
  },
  switchButtonText: {
    color: '#ffd280',
    marginLeft: '4px',
    fontWeight: 500,
  },
}));

export default useStyles;
