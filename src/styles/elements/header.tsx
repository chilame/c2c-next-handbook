import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  nav: {
    position: 'fixed',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: '80px',
    padding: '0 19px 0 41px',
    backgroundColor: 'rgba(255, 255, 255, 0.98)',
    zIndex: 1,
    boxShadow: '0 0.5px 0 0 rgba(0, 0, 0, 0.15)',
    [theme.breakpoints.down('sm')]: {
      height: '56px',
      padding: '0 8px 0 16px',
    },
  },
  logo: {
    objectFit: 'contain',
    cursor: 'pointer',
  },
  hamburger: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    padding: '20px 12px 16px',
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.04)',
    },
    [theme.breakpoints.down('sm')]: {
      paddingTop: '16px',
    },
  },
  headerItem: {
    padding: '20px 12px 16px',
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.04)',
    },
    [theme.breakpoints.down('sm')]: {
      paddingTop: '16px',
    },
  },
  links: {
    display: 'flex',
  },
  link: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  linkText: {
    fontSize: '12px',
    fontWeight: 500,
    color: '#608099',
    marginTop: '5px',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
}));

export default useStyles;
