import { Theme, createStyles } from '@material-ui/core/styles';

const globalStyles = (theme: Theme) =>
  createStyles({
    '@global': {
      body: {
        margin: '0px',
        fontFamily: 'ff-meta-serif-web-pro, serif',
        color: 'rgb(29, 45, 53)',
        backgroundColor: 'rgba(251, 247, 240)',
      },
      '*': {
        boxSizing: 'border-box',
        margin: '0',
        padding: '0',
      },
      a: {
        color: 'rgb(108, 63, 153)',
        textDecoration: 'underline',
        cursor: 'pointer',
      },
      p: {
        margin: '16px 0 0',
        fontSize: '21px',
      },
      hr: {
        margin: '48px 0',
        border: '0',
        borderTop: '1.6px solid rgb(29, 45, 53)',
      },
      h2: {
        fontSize: '42px',
        lineHeight: '1.2',
        marginTop: '63px',
      },
      h3: {
        fontSize: '33px',
        lineHeight: '1.2',
        marginTop: '47px',
      },
      h4: {
        fontSize: '25px',
        lineHeight: '1.2',
        marginTop: '38px',
      },
      ul: {
        margin: '20px 0 0 20px',
        lineHeight: '1.5',
      },
      li: {
        fontSize: '21px',
      },
    },
  });

export default globalStyles;
