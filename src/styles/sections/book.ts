import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  bookSection: {
    display: 'flex',
    padding: '32px',
  },
  leftSide: {
    minWidth: '25%',
  },
  menu: {
    position: 'fixed',
    maxWidth: '25%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  rightSide: {
    padding: '42px 96px 0 56px',
  },
  title: {
    fontSize: '56px',
    fontWeight: 'bold',
    marginBottom: '14px',
    lineHeight: '1.1',
  },
  subTitle: {
    fontSize: '36px',
    lineHeight: '1.2',
  },
  bookContent: {
    listStyleType: 'none',
  },
  chapterContent: {
    margin: '16px 0',
  },
  chapterNumber: {
    margin: '48px 0 0',
    textTransform: 'uppercase',
    letterSpacing: '5px',
    fontSize: '14px',
  },
  chapterTitle: {
    fontSize: '42px',
    margin: '0',
    textAlign: 'right',
    color: 'rgb(108, 63, 153)',
  },
  chapterName: {
    margin: '0',
    fontSize: '25px',
  },
  alignRight: {
    textAlign: 'right',
  },
  nextChap: {
    marginTop: '21px',
    lineHeight: '1.25',
    fontWeight: 'bold',
    fontSize: '21px',
    color: 'rgb(251, 247, 240)',
    backgroundColor: 'rgb(108, 63, 153)',
    padding: '10px 16px',
    border: '1px solid rgb(108, 63, 153)',
    borderRadius: '20px',
    fontFamily: 'ff-meta-serif-web-pro, serif',
    cursor: 'pointer',
  },
}));

export default useStyles;
