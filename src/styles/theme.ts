import { createMuiTheme, Theme, responsiveFontSizes } from '@material-ui/core/styles';

declare module '@material-ui/core/styles/createPalette' {
  interface Palette {
    tetriary: PaletteColor;
    contrast: PaletteColor;
    hyperlink: PaletteColor;
    contactTextColor: PaletteColor;
    contactTextError: PaletteColor;
  }
  interface PaletteOptions {
    tetriary?: PaletteColorOptions;
    contrast?: PaletteColorOptions;
    hyperlink?: PaletteColorOptions;
    contactTextColor?: PaletteColorOptions;
    contactTextError?: PaletteColorOptions;
  }
}

const muiTheme: Theme = createMuiTheme({
  palette: {},
  breakpoints: {
    values: {
      xs: 0,
      sm: 374,
      md: 800,
      lg: 1024,
      xl: 1920,
    },
  },
});

export default responsiveFontSizes(muiTheme);
