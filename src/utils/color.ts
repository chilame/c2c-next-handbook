import { textColorInterface } from './types';

type GetTextColorReturn = { color: string | undefined } | undefined;

export function getTextColor(textColorObj: textColorInterface): GetTextColorReturn {
  if (textColorObj == null) {
    return { color: `rgb(50, 68, 82)` };
  }

  const { textColor } = textColorObj;
  return { color: `${textColor.hex}` };
}
