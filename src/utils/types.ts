export interface linkInterface {
  id: string | number;
  url: string;
  text: string;
  newTab?: boolean;
}

export interface mediaInterface {
  id: string | number;
  alternativeText?: string;
  mime: string;
  url: string;
}

export interface menuInterface {
  links: linkInterface[];
  customer: linkInterface;
  therapist: linkInterface;
}

export interface backgroundColorInterface {
  backgroundColor: {
    backgroundType: string;
    rgba?: string;
    linear?: string;
  };
}

export interface textColorInterface {
  textColor: {
    hex?: string;
  };
}

export interface imageInterface {
  url: string;
  width?: number;
  height?: number;
  alt?: string;
}

export interface metadataInterface {
  metaTitle?: string;
  metaDescription?: string;
  shareImage?: {
    formats: {
      [key: string]: imageInterface;
    };
  };
  twitterCardType?: string;
  twitterUsername?: string;
}
